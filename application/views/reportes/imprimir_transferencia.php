<?php $usuario = $this->db->get_where('user',array('id'=>$venta->user_id)); ?>  
        
            <p>TRANSFERENCIA</p>
            <p>Comprobante de Trasferencia de Artículos de una sucursal a otra	</p>
            <table border="0" cellspacing="2" cellpading="2">                
                <tr><td><b>Sucursal Origen: </b></td><td><?= $venta->sucursalOrigen ?></td><td><b>Sucursal Destino: </b></td><td><?= $venta->sucursalDestino ?></td></tr>                
                <tr><td><b>Fecha: </b></td><td><?= date("d-m-Y",strtotime($venta->fecha_solicitud)) ?></td><td><b>Hora: </b></td><td><?= date("H:i:s",strtotime($venta->fecha_solicitud)) ?></td></tr>                
                <tr><td><b>Usuario: </b></td><td><?= $usuario->num_rows>0?$usuario->row()->nombre.' '.$usuario->row()->apellido:'N/A'; ?></td></tr>                
            </table>
            <br/>
         
            <table   border="1" cellspacing="2" cellpading="2">                
                <thead>
                    <tr>
                        <th>Cod. Artículo</th>
                        <th>Nombre Artículo</th>
                        <th>Venc.</th>
                        <th>Lote</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                    </tr>
                </thead>
                <tbody>                    
                    <?php foreach($detalles->result() as $d): ?>
                    <?php
                        //traer vencimiento
                        if(!empty($d->lote)){
                            $this->db->where('productosucursal.lote',$d->lote);
                        }
                        $this->db->where('productosucursal.producto',$d->producto);
                        $this->db->where('productosucursal.sucursal',$venta->sucursalOrigenId);
                        $productosucursal = $this->db->get('productosucursal');
                        $d->vencimiento = $productosucursal->num_rows==0?'N/A':$productosucursal->row()->vencimiento;
                    ?>
                    <tr>
                        <td><?= $d->producto ?></td>
                        <td><?= cortar_palabras($d->nombre_comercial,5) ?></td>
                        <td><?= date("d-m-Y",strtotime($d->vencimiento)) ?></td>
                        <td><?= $d->lote ?></td>
                        <td><?= $d->cantidad ?></td>
                        <td><?= number_format($d->precio_venta,2,',','.'); ?></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
          
            <table cellspacing="5" cellpading="5">
                    <tr>
                    	<td style="text-align:center"><div style="margin:5px">___________________ <br/> Cajero que envió</div></td>
                    	<td style="text-align:center"><div style="margin:5px">___________________ <br/> Cajero que recibió</div></td>
                        <td style="text-align:center"><div style="margin:5px">___________________ <br/> Administrador</div></td>
                        
                        
                    </tr>
            </table>
