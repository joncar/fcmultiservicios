  <ol class="breadcrumb">
    <li><a href="<?= site_url() ?>">Inicio</a></li>
    <li><a href="<?= site_url($this->router->fetch_class()) ?>"><?= ucfirst($this->router->fetch_class()) ?></a></li>    
    <li class="active">
        <a href="<?= site_url($this->router->fetch_class().'/'.$this->router->fetch_method()) ?>"><?= !empty($title)?$title:ucfirst($this->router->fetch_method()) ?></a>
    </li>    
  </ol>