<div class="col-xs-12 row">
    <article>
    <h1>Reportes</h1>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Clientes
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
            <div class="list-group">
                <a class="list-group-item" href="<?= base_url('reportes/saldos') ?>"><i class="fa fa-print"></i> <br/> Saldos de clientes</a>
                <a class="list-group-item" href="<?= base_url('reportes/listado_ventas_clientes_creditos') ?>"><i class="fa fa-print"></i> <br/> Resumen de pagos de clientes</a>
                <a class="list-group-item" href="<?= base_url('admin') ?>"><i class="fa fa-print"></i> <br/> Detalles de compras de clientes</a>
                <a class="list-group-item" href="<?= base_url('reportes/movimientos_clientes') ?>"><i class="fa fa-print"></i> <br/> Movimiento de  clientes</a>
            </div>
        </div>
      </div>
    </div>  

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
            Proveedores
          </a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
            <div class="list-group">
                <a class="list-group-item" href="<?= base_url('reportes/obligaciones_proveedor') ?>"><i class="fa fa-print"></i> <br/> Obligaciones a pagar</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_compras') ?>"><i class="fa fa-print"></i> <br/> Resumen de compras por Proveedor</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_descuentos') ?>"><i class="fa fa-print"></i> <br/> Resumen de porcentajes de descuentos de las compras a distribuidoras</a>
            </div>
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
            Ventas
          </a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
            <div class="list-group">
                <a class="list-group-item" href="<?= base_url('reportes/ventas_sucursal') ?>"><i class="fa fa-print"></i> <br/> Ventas por Sucursal</a>
                <a class="list-group-item" href="<?= base_url('reportes/listado_ventas_clientes') ?>"><i class="fa fa-print"></i> <br/> Ventas por Cliente</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_ventas') ?>"><i class="fa fa-print"></i> <br/> Cantidad vendida de Productos</a>
                <a class="list-group-item" href="<?= base_url('reportes/detalle_ventas') ?>"><i class="fa fa-print"></i> <br/> Detalle de venta por día</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_ventas_clientes') ?>"><i class="fa fa-print"></i> <br/> Resumen de ventas por cliente</a>
                <a class="list-group-item" href="<?= base_url('reportes/pagos_clientes') ?>"><i class="fa fa-print"></i> <br/> Resumen de pagos por cliente</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_anual') ?>"><i class="fa fa-print"></i> <br/> Resumen anual</a>
                <a class="list-group-item" href="<?= base_url('reportes/resumen_facturas_contador') ?>"><i class="fa fa-print"></i> <br/> Resumen de facturas para contador</a>
            </div>
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
            Productos
          </a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
            <div class="list-group">
                <a class="list-group-item" href="<?= base_url('reportes/vencimientos') ?>"><i class="fa fa-print"></i> <br/> Listado de productos con vencimiento</a>
                <a class="list-group-item" href="<?= base_url('reportes/distribucion') ?>"><i class="fa fa-print"></i> <br/> Inventario valorizado</a>                
                <a class="list-group-item" href="<?= base_url('reportes/listado_inventario') ?>"><i class="fa fa-print"></i> <br/> Listado de inventario</a>
                <a class="list-group-item" href="<?= base_url('reportes/listado_inventario_total') ?>"><i class="fa fa-print"></i> <br/> Listado de inventario General</a>
                <a class="list-group-item" href="<?= base_url('reportes/listado_inventario_categoria') ?>"><i class="fa fa-print"></i> <br/> Listado de inventario por categoría</a>
                <a class="list-group-item" href="<?= base_url('reportes/inventario_valorizado') ?>"><i class="fa fa-print"></i> <br/> Inventario Valorizado</a>
            </div>
        </div>
      </div>
    </div>
</div>
</article>
</div>