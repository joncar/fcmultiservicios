<body class="no-skin">
        <?php $this->load->view('includes/header') ?>
    <div class="main-container" id="main-container">
        <?php $this->load->view('includes/sidebar') ?>
        <div class="main-content">
            <div class="main-content-inner">
                <?php $this->load->view('includes/breadcum') ?>
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            <?= empty($title) ? 'Escritorio' : $title ?>
                            <small>
                                <i class="ace-icon fa fa-angle-double-right"></i>
                                <span><b>Sucursal: </b> <a href="<?= base_url('panel/selsucursal') ?>"><?= !empty($_SESSION['sucursalnombre'])?$_SESSION['sucursalnombre']:'Sin Seleccionar'  ?></a> </span>
                                <span><b>Caja: </b> <a href="<?= base_url('panel/selcaja') ?>"><?=  !empty($_SESSION['cajanombre'])?$_SESSION['cajanombre']:'Sin Seleccionar'  ?> </a> </span>
                            </small>
                        </h1>
                    </div><!-- /.page-header -->

                    <div class="row">
                        <div class="col-xs-12">            
                            <?= empty($crud) ? $this->load->view("includes/template/dashboard"): $this->load->view('cruds/' . $crud) ?>                
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div>
        </div><!-- /.main-content -->			
    </div><!-- /.main-container -->
    <script src="<?= base_url("js/ace.min.js") ?>"></script>
    <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
    <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
</body>