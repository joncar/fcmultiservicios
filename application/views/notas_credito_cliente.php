<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">        
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-6 control-label">Nro. Venta: </label>
              <div class="col-sm-6" id="cliente_div">      
                  <? $sel = empty($nota)?0:$nota->compra; ?>
                  <? $this->db->where('ventas.status','-1'); ?>
                  <?= form_dropdown_from_query('venta','ventas','id','nro_factura',$sel,'id="venta"') ?>
              </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-6 control-label">Total Monto: </label>
              <div class="col-sm-6" id="cliente_div">      
                  <? $sel = empty($nota)?0:$nota->total_monto; ?>                  
                  <input type="text" name="total_monto" value="<?= empty($nota)?'':$nota->total_monto ?>" id="total_monto" class="form-control">
              </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-6 control-label">Sucursal: </label>
              <div class="col-sm-6" id="cliente_div">      
                  <? $sel = empty($nota)?0:$nota->sucursal; ?>                  
                  <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',$sel,'id="sucursal"') ?>
              </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Nota Credito: </label>
              <div class="col-sm-8" id="cliente_div">
                  <? $sel = empty($nota)?0:$nota->motivo; ?>
                  <input type="text" name="nro_nota_credito" value="<?= empty($nota)?'':$nota->nro_nota_credito ?>" id="nro_nota_credito" class="form-control">
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($nota)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($nota->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div>    
    </div>
    </div>
    <div class="row">
        <div class="col-xs-12">Detalle de salida <a href="javascript:advancesearch()" class="btn btn-default">Busqueda avanzada de productos</a></div>
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2">
                <thead>
                    <tr>
                        <th style="width:18%">Código</th>
                        <th style="width:41%">Nombre artículo</th>
                        <th style="width:7%">Lote</th>                        
                        <th style="width:9%">Cant.</th>                        
                        <th style="width:9%">P.Venta</th>
                        <th style="width:10%">Total</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($detalles)): foreach($detalles->result() as $d): ?>
                    <tr>
                        <td><input name="codigo[]" type="text" value='<?= $d->producto ?>' class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" value='<?= $this->db->get_where('productos',array('codigo'=>$d->producto))->row()->nombre_comercial ?>'  class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" value='<?= $d->lote ?>'  class="form-control lote" placeholder="Lote"></td>                        
                        <td><input name="cantidad[]" type="text" value='<?= $d->cantidad ?>'  class="form-control cantidad" placeholder="Cantidad"></td>                        
                        <td><input name='precio_venta[]' type="text" value='<?= $d->precio_venta ?>'  class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" value='<?= $d->total ?>'  class="form-control total" placeholder="Total">                        
                        </td>                                                
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                    <?php  endforeach; endif ?>
                    <tr>
                        <td><input name="codigo[]" type="text" class="form-control codigo" placeholder="Codigo"></td>
                        <td><input name="producto[]" type="text" class="form-control producto" readonly placeholder="Nombre"></td>
                        <td><input name="lote[]" type="text" class="form-control lote" placeholder="Lote"></td>                        
                        <td><input name="cantidad[]" type="text" class="form-control cantidad" placeholder="Cantidad"></td>                        
                        <td><input name='precio_venta[]' type="text"   class="form-control precio_venta" placeholder="Precio venta"></td>
                        <td><input name='total[]' type="text" value=''  class="form-control total" placeholder="Total"></td>                        
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>   
    <?php if(!empty($nota)): ?><input type="hidden" name="id" id="id" value="<?= $nota->id ?>"><?php endif ?>
    <div class="row" style="margin-top:40px">                
        <button id="guardar" type="submit" class="btn btn-success">Guardar Entrada</button>
        <a href="#" class="btn btn-default">Imprimir Reporte</a>
    </div>
</form>
<script>
    var json = undefined;
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
            $(".hasDatepicker").removeAttr('id').removeClass('hasDatepicker');            
            date_init_calendar();
    }
    function removerow(obj){
        $(obj).parent('td').parent('tr').remove();
        $(document).trigger('total');
    }
    
    function tryagain(){
        $.post('<?= base_url('json/clientes') ?>',{},function(data){
            
            $("#cliente_div #cliente_chzn").remove();
            $("#cliente").html(data);
            $("#cliente").removeClass('chzn-done');            
            $(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
        });
    }
    
    //Eventos
    $(document).on('keydown','input',function(event){        
            
        if (event.which == 13){
            if($(this).hasClass('total')){
                addrow($(this));
            }
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } 
            else if($(this).val()!='' && $(this).hasClass('codigo')){
                $(this).trigger('change');                
            }
            else if($(this).val()=='' && $(this).hasClass('codigo')){
                return false;
            }
            else
            {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    })

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
   $("body").on('change','.codigo',function(e){
       if($(this).val()!=''){
        focused = $(this);
        elements = $(".codigo").length;
        focusedcode = $(this).val();        
        if(json==undefined){
            
        json = $.post('<?= base_url('json/getProduct') ?>',{codigo:$(this).val()},function(data){
            data = JSON.parse(data);
            data = data['producto'];
            if(data!=''){
            focused = focused.parent().parent();
            focused.find('.producto').val(data['nombre_comercial'])
            focused.find('.cantidad').val(1);
            focused.find('.total').val(data['precio_venta']);
            focused.find('.precio_venta').val(data['precio_venta']);
            $(document).trigger('total');
            addrow(focused.find('a'));
            $("tbody tr").last().find('.codigo').focus();            
            }
            else{
                emergente('El producto ingresado no se encuentra registrado. <a target="_new" href="<?= base_url($this->router->fetch_class().'/productos/add/json') ?>/'+focused.val()+'">¿Desea registrar uno nuevo?</a>');                        
                
            }
            json = undefined
        });        
        }
        }
    });
    
    $(document).on('change','.cantidad',function(){$(document).trigger('total')});
    
    $(document).on('total',function(){      
        $("#total_monto").val(0);                
        $("tbody tr").each(function(){
            self = $(this);            
            total = parseInt(self.find('.cantidad').val())*parseInt(self.find('.precio_venta').val());                                    
            if(!isNaN(total)){
                self.find('.total').val(total);
                /*Total Venta*/
                total_venta = parseInt($("#total_monto").val());            
                total_venta += total;
                $("#total_monto").val(total_venta);
            }
        });
    });
    
    function val_send(form){
        $(".mask").show();
        var data = document.getElementById('formulario');
        $(document).trigger('total')
        data = new FormData(data);
        $("#guardar").attr('disabled','disabled');
        $.ajax({
            url:'<?= empty($nota)?base_url('json/notascreditocliente'):base_url('json/notascreditocliente/edit') ?>',
            method:'post',
            data:data,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
                data = JSON.parse(data);                
                if(data['status']){
                    document.location.reload();
                }
                else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data['message']).show();
                    $(".mask").hide();
                }
                $("#guardar").removeAttr('disabled');
            },
            error:function(){
                $(".mask").hide();
            }
            });
            return false;
    }
    
    function advancesearch()
    {
        $.post('<?= base_url('json/searchProduct/0') ?>',{},function(data){
            emergente(data);
        })
    }
    
    function selCod(cod,lote,datos){
        
        $("tbody tr:last-child").find('.codigo').val(cod);
        $("tbody tr:last-child").find('.lote').val(lote);
        $("tbody tr:last-child").find('.cantidad').val(datos['cantidad']);
        $("tbody tr:last-child").find('.vencimiento').val(datos['vencimiento']);
        $("tbody tr:last-child").find('.codigo').trigger('change');        
        $("tbody tr:last-child").find('.precio_venta').val(datos['precio_venta']);        
        $('#myModal').modal('hide');
    }    
</script>
<?php $this->load->view('predesign/datepicker') ?>
<?= $this->load->view('predesign/chosen.php') ?>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>