<?php

class Barcode{
    /*
    Codificación seguida para representar el código de barras:
       - Las cifras se representan por (la cifra + 5) en binario natural
       - Cifra 5 al principio (|-|-) más 2 espacios a mayores de lo normal entre cifras
       - 4 unidades por dígito, y 2 de espaciado entre cada dígito.
       - 3 espacios tras el último número, y 1011.
    */
    function draw($numero, $altura = 40,$size = '200px'){
        $this->altura = $altura;   //altura que tendrán los códigos de barras
        $this->cod = $numero; //ENTERO a representar
        $this->dimensiones = $this->tamano($this->cod, $this->altura);
        $this->imagen = imagecreate($this->dimensiones['x'], $this->dimensiones['y']);
        $this->blanco = imagecolorallocate($this->imagen, 255, 255, 255);
        $this->negro = imagecolorallocate($this->imagen, 0, 0, 0);
        imagefill($this->imagen, 0, 0, $this->blanco);
        imagerectangle($this->imagen, 0, 0, imageSX($this->imagen) - 1, imageSY($this->imagen) - 1, $this->negro);
        $this->codigo($this->cod, $this->altura);
        imagepng($this->imagen,'img/barcode.png');
        return '<img style="width:'.$size.';" src="'.base_url('img/barcode.png').'">';
    }    

    function tamano($numero, $altura){
       $cifras = strlen($numero) + 1;
       $dim['x'] = 7 + $cifras*6 + 9;
       $dim['y'] = $altura + 1;
       return $dim;
    }       

    function cifra($num){
       return str_pad(decbin ($num + 5),4,'0',STR_PAD_LEFT);
    }

    function barra($y2, $x_ini, $codigo){
       for ($i = 0; $i <= 3; $i++){

          if ($codigo[$i] == 0){ 
             $color = $this->blanco; 
          } else { 
             $color = $this->negro; 
          }

          $x = $x_ini + $i;

          imageline($this->imagen, $x, 5, $x, $y2, $color);
       }
    }


    function codigo($numero){       
       $x = 5;
       $this->barra ($this->altura - 5, $x, "1010");
       $x = $x + 7;


       for ($e = 0; $e <= strlen($numero) - 1; $e++){
          $this->barra ($this->altura - 15, $x, $this->cifra($numero[$e]));
          imagestring($this->imagen, 2, $x, $this->altura - 15, $numero[$e], $this->negro);
          $x = $x + 6;
       }

       $x = $x + 1;
       $this->barra ($this->altura - 5, $x, "1011");

    }

    
}