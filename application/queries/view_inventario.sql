DROP VIEW IF EXISTS view_inventario;
CREATE VIEW view_inventario AS SELECT 
productosucursal.id,
producto as Codigo,
proveedores.denominacion as Laboratorio,
DATE_FORMAT(productosucursal.fechaalta,'%d/%m/%Y %H:%i') AS 'Fecha registro',
productos.nombre_comercial,
sucursales.denominacion AS sucursal,
sucursales.id AS sucursal_id,
productosucursal.lote,
DATE_FORMAT(productosucursal.vencimiento,'%d/%m/%Y') AS vencimiento,
get_stock(producto,lote,sucursal) AS stock,
productos.precio_venta
FROM productosucursal
INNER JOIN productos on productos.codigo = productosucursal.producto
INNER JOIN proveedores ON proveedores.id = productos.proveedor_id
INNER JOIN sucursales ON sucursales.id = productosucursal.sucursal;