BEGIN
DECLARE stock INT;
DECLARE compras INT;
DECLARE transferencias_in INT;
DECLARE entrada_productos INT;
DECLARE ventas INT;
DECLARE salidas INT;
DECLARE transferencias_ou INT;
SELECT 
IFNULL(SUM(cantidad),0) into compras
FROM compradetalles 
INNER JOIN compras ON compras.id = compradetalles.compra 
WHERE producto = productoV AND compras.sucursal = sucursalV AND compradetalles.lote = loteV AND compras.status = 0;

SELECT
IFNULL(SUM(cantidad),0) into transferencias_in
FROM transferencias_detalles
INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia
WHERE producto = productoV AND transferencias.sucursal_destino = sucursalV AND transferencias_detalles.lote = loteV AND transferencias.procesado = 2;

SELECT
IFNULL(SUM(cantidad),0) INTO entrada_productos
FROM entrada_productos_detalles 
WHERE entrada_productos_detalles.producto = productoV AND sucursal = sucursalV AND entrada_productos_detalles.lote = loteV;

SELECT
IFNULL(SUM(cantidad),0) INTO ventas
FROM ventadetalle 
INNER JOIN ventas ON ventas.id = ventadetalle.venta
WHERE ventadetalle.producto = productoV AND ventas.sucursal = sucursalV AND ventadetalle.lote = loteV AND (ventas.status IS NULL OR ventas.status = 0);

SELECT 
IFNULL(SUM(cantidad)*-1,0) INTO salidas
FROM salida_detalle
WHERE salida_detalle.producto = productoV AND salida_detalle.sucursal = sucursalV AND salida_detalle.lote = loteV;

SELECT
IFNULL(SUM(cantidad),0) INTO transferencias_ou
FROM transferencias_detalles 
INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia
WHERE producto = productoV AND transferencias.sucursal_origen = sucursalV AND transferencias_detalles.lote = loteV AND transferencias.procesado = 2;

SELECT (compras+transferencias_in+entrada_productos)-(ventas+salidas+transferencias_ou) INTO stock;
RETURN stock;
END