<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Querys extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    function fecha($val)
    {
        return date($this->ajustes->formato_fecha,strtotime($val));
    }
    
    function moneda($val)
    {
        return $val.' '.$ajustes->moneda;
    }
    
    //Valida si tiene acceso a algun controaldor
    function has_access($controller,$function = '')
    {
        if(!is_numeric($controller))
            $con = $this->db->get_where('controladores',array('nombre'=>$controller));
        
        if(!empty($function) && !is_numeric($function) && (!empty($con) && $con->num_rows()>0 || is_numeric($controller)))
            $fun = $this->db->get_where('funciones',array('nombre'=>$function,'controlador'=>$con->row()->id));
        
        $controller = !empty($con) && $con->num_rows()>0?$con->row()->id:$controller;
        $function = !empty($fun) && $fun->num_rows()>0?$fun->row()->id:$function;        
        
        if($_SESSION['cuenta']!=1)
        {            
            if(!empty($function))
            return $this->db->get_where('roles',array('controlador'=>$controller,'funcion'=>$function,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
            else
            return $this->db->get_where('roles',array('controlador'=>$controller,'user'=>$_SESSION['user']))->num_rows()>0?TRUE:FALSE;
        }
        else
            return TRUE;
    }
    
    function get_menu($controlador,$returnforeach = FALSE)
    {
        $str = '';
        $this->db->select('controladores.id as controlador,funciones.etiqueta as label, funciones.id as funcion_id, controladores.nombre, funciones.nombre as funcion');
        $this->db->join('funciones','funciones.controlador = controladores.id');
        $menu = $this->db->get_where('controladores',array('controladores.nombre'=>$controlador,'funciones.visible'=>1));
        if($menu->num_rows()>0 && $this->has_access($menu->row()->controlador)){
        $str .= '';
        foreach($menu->result() as $m){
        if($this->has_access($m->controlador,$m->funcion_id)){
        $label = empty($m->label)?ucwords($m->funcion):$m->label;
        $str.= '<li><a href="'.base_url($controlador.'/'.$m->funcion).'">'.$label.'</a></li>';
        }
        }
        $str.= '';
        }
        if(!$returnforeach)
        return $str;
        else return $menu->result();
    }
    //Funcion para saber cuantos productos hay disponibles para distribuir entre las sucursales de la compra realizada
    function get_disponible($compra){
        
            $this->db->select('SUM(distribucion.cantidad) as p');
            $productosucursal = $this->db->get_where('distribucion',array('compra'=>$compra));
            
            $this->db->select('SUM(compradetalles.cantidad) as p');
            $compradetalles = $this->db->get_where('compradetalles',array('compra'=>$compra));
            
            if($compradetalles->num_rows()>0 && $productosucursal->num_rows()>0)
            return $compradetalles->row()->p-$productosucursal->row()->p;
            
    }
    
    //Traer el nuevo nro de factura para realizar una venta en la caja seleccionada.
    function get_nro_factura(){
        $sucursal = $_SESSION['sucursal'];
        if(empty($_SESSION['caja'])){
            $caja = $this->db->get_where('cajas',array('sucursal'=>$sucursal));            
        }
        else
            $caja = $this->db->get_where('cajas',array('id'=>$_SESSION['caja']));
        if($caja->num_rows()>0){
            $caja = $caja->row();
            if(empty($_SESSION['caja']))$_SESSION['caja'] = $caja->id;
            $serie = $caja->serie;
            
            $ultima_venta = $this->db->get_where('cajadiaria',array('caja'=>$caja->id,'abierto'=>1));
            $ultima_venta = $serie.($ultima_venta->row()->correlativo+1);
            return $ultima_venta;
        }
        else
            header("Location:".base_url('panel/selsucursal').'?redirect='.$this->router->fetch_class().'/ventas');
    }

    function validar_stock(){
        /*$reporte = $this->db->get_where('newreportes',array('id'=>63));
        if($reporte->num_rows()>0){
            $reporte = $reporte->row();
            $reporte->query = $this->cleanData($reporte->query);
            $reporte->query = str_replace('consulta=','',$reporte->query);
            $reporte->query = trim($reporte->query);
            $reporte->query = str_replace('|selec|','SELECT',$reporte->query); //Quitamos la validación XSS
            $this->db->query($reporte->query);*/
            /*if(empty($this->user->sucursal)){            

            	return $this->db->get_where('user',array('id'=>'-1'));
            }

            $consult = "
               SELECT
                *,
                round((verificacion.stock_real -verificacion.stock_tabla_prod_sucursal),0) as diferencia,
                if(verificacion.stock_real !=verificacion.stock_tabla_prod_sucursal,1,0) as auditoria
                FROM(

                SELECT 
                producto_sucursal.prodid as prodid,
                stock_g.prod as Prod_Suc_Lote, 
                stock_g.lote as Lote,
                productos.nombre_comercial as Producto_nombre, 
                stock_g.cod_producto as Cod_producto, 
                round(sum(stock_g.Compra),0) as Compra, 
                round(sum(stock_g.Ventas),0) as Venta, 
                round(sum(stock_g.Ajuste_Entrada),0) as Aj_entrada, 
                round(sum(stock_g.Ajuste_Salida),0) as Aj_salida, 
                round(sum(stock_g.Transferencia_Salida),0) as Transf_salida, 
                round(sum(stock_g.Transferencia_Entrada),0) as Transf_entrada, 
                round((sum(stock_g.Compra)+sum(stock_g.Ajuste_Entrada)+sum(stock_g.Transferencia_Entrada)),0) as T_entrada, 
                round((sum(stock_g.Ventas)+sum(stock_g.Ajuste_Salida)+sum(stock_g.Transferencia_Salida)),0) as T_salida, 
                round((sum(stock_g.Compra)+sum(stock_g.Ajuste_Entrada)+sum(stock_g.Transferencia_Entrada)) - (sum(stock_g.Ventas)+sum(stock_g.Ajuste_Salida)+sum(stock_g.Transferencia_Salida)),0) as stock_real,
                round(producto_sucursal.stock) as stock_tabla_prod_sucursal
                FROM (SELECT 
                concat(movimiento.sucursal,'-',upper(movimiento.cod_producto),'-',movimiento.lote) as prod, 
                movimiento.lote as lote,
                movimiento.sucursal as sucursal, 
                upper(movimiento.cod_producto) as cod_producto, 
                if(movimiento.Movimiento = 'compra',Cantidad,0) as Compra, 
                if(movimiento.Movimiento = 'venta',Cantidad,0) as Ventas, 
                if(movimiento.Movimiento = 'ajuste_entrada',Cantidad,0) as Ajuste_Entrada, 
                if(movimiento.Movimiento = 'ajuste_salida',Cantidad,0) as Ajuste_Salida,
                if(movimiento.Movimiento = 'trasferencia_salida',Cantidad,0) as Transferencia_Salida, 
                if(movimiento.Movimiento = 'trasferencia_entrada',Cantidad,0) as Transferencia_Entrada 
                FROM(
                SELECT 
                'trasferencia_salida' as Movimiento, 
                transferencias.sucursal_origen as sucursal, 
                transferencias.id as nro_registro, 
                transferencias.fecha_solicitud as fecha, 
                productos.codigo as cod_producto, 
                if(transferencias_detalles.lote='',0,transferencias_detalles.lote) as lote,
                transferencias_detalles.cantidad as cantidad 
                FROM transferencias 
                INNER JOIN transferencias_detalles on transferencias_detalles.transferencia = transferencias.id 
                INNER JOIN productos on productos.codigo = transferencias_detalles.producto 
                WHERE transferencias.procesado = 2 
                UNION ALL 
                SELECT 
                'trasferencia_entrada' as Movimiento, 
                transferencias.sucursal_destino as sucursal, 
                transferencias.id as nro_registro, 
                transferencias.fecha_solicitud as fecha, 
                productos.codigo as cod_producto, 
                if(transferencias_detalles.lote='',0,transferencias_detalles.lote) as lote,
                transferencias_detalles.cantidad as cantidad 
                FROM transferencias 
                INNER JOIN transferencias_detalles on transferencias_detalles.transferencia = transferencias.id 
                INNER JOIN productos on productos.codigo = transferencias_detalles.producto 
                WHERE transferencias.procesado = 2 
                UNION ALL 
                SELECT 
                'venta' as Movimiento, 
                ventas.sucursal as sucursal, 
                ventas.id as Nro_registro, 
                ventas.fecha as Fecha, 
                productos.codigo as cod_producto, 
                if(ventadetalle.lote='',0,ventadetalle.lote) as lote,
                ventadetalle.cantidad as Cantidad 
                FROM ventas 
                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id 
                INNER JOIN productos on productos.codigo = ventadetalle.producto 
                WHERE (ventas.status = 0 or ventas.status is null) 
                UNION ALL 
                SELECT 
                'compra' as Movimiento, 
                compras.sucursal as sucursal, 
                compras.id as Nro_registro, 
                compras.fecha as Fecha, 
                productos.codigo as cod_producto, 
                if(compradetalles.lote='',0,compradetalles.lote) as lote,
                compradetalles.cantidad as Cantidad 
                FROM compras 
                INNER JOIN compradetalles on compradetalles.compra = compras.id 
                INNER JOIN productos on productos.codigo = compradetalles.producto 
                WHERE (compras.status = 0 or compras.status is null) 
                UNION ALL 
                SELECT 
                'ajuste_entrada' as Movimiento, 
                entrada_productos_detalles.sucursal as sucursal, 
                entrada_productos.id as Nro_registro, 
                entrada_productos.fecha as Fecha, 
                productos.codigo as cod_producto, 
                if(entrada_productos_detalles.lote='',0,entrada_productos_detalles.lote) as lote,
                entrada_productos_detalles.cantidad as Cantidad 
                FROM entrada_productos 
                INNER JOIN entrada_productos_detalles on entrada_productos_detalles.entrada_producto = entrada_productos.id 
                INNER JOIN productos on productos.codigo = entrada_productos_detalles.producto 
                UNION ALL 
                SELECT 
                'ajuste_salida' as Movimiento, 
                salida_detalle.sucursal as sucursal, 
                salidas.id as Nro_registro, 
                salidas.fecha as Fecha, 
                productos.codigo as cod_producto, 
                if(salida_detalle.lote='',0,salida_detalle.lote) as lote,
                salida_detalle.cantidad as Cantidad 
                FROM salidas 
                INNER JOIN salida_detalle on salida_detalle.salida = salidas.id 
                INNER JOIN productos on productos.codigo = salida_detalle.producto) as movimiento) AS stock_g 
                INNER JOIN productos on productos.codigo = stock_g.cod_producto 
                INNER JOIN (
                    SELECT
                    inventario.prodid,
                    inventario.suc_prod_lote as suc_prod_lote,
                    inventario.sucursal as sucursal,
                    upper(inventario.cod_prod) as cod_prod,
                    sum(inventario.stock) as stock
                    FROM(
                    SELECT 
                    p.id as prodid,
                    concat(p.sucursal,'-',upper(p.producto),'-',if(p.lote='',0,p.lote)) as suc_prod_lote,
                    p.sucursal as sucursal,
                    p.producto as cod_prod,
                    p.stock
                    FROM productosucursal p) AS inventario
                    GROUP BY inventario.suc_prod_lote
                    ORDER BY inventario.cod_prod

                ) as producto_sucursal on producto_sucursal.suc_prod_lote = stock_g.prod
                GROUP by stock_g.prod) verificacion 
                WHERE if(verificacion.stock_real !=verificacion.stock_tabla_prod_sucursal,1,0) = 1
            ";
            $query = $this->db->query($consult);
            if($query->num_rows()>0){
                //return $this->db->get_where('user',array('id'=>'-1')); 
                $this->ajustar_stock($query);
            }
        }*/
    }

    function ajustar_stock($query){
        foreach($query->result() as $q){
            //$this->db->update('productosucursal',array('stock'=>$q->stock_consulta),array('id'=>$q->idstock));
            $this->db->update('productosucursal', array('stock'=>$q->stock_real), array('id'=>$q->prodid));
        }
    }
}
?>
