<?php
require_once APPPATH.'/controllers/main.php';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ver");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud_1');                
                $this->load->model('querys');
                /*if(empty($_SERVER['HTTP_VER'])){          
                    echo 'denied';
                    die();
                } */
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD_1($controller);
            $crud->set_theme('bootstrap2');
            //$crud->set_model('api_model');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function user(){
            if(!empty($_POST['search_field'])){
                foreach($_POST['search_field'] as $n=>$p){
                    if($p=='password'){
                        $_POST['search_text'][$n] = md5($_POST['search_text'][$n]);
                    }
                }
            }
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function productos(){
            $crud = $this->crud_function('','');
            $crud->columns('id','codigo','nombre_comercial','nombre_generico','descripcion','propiedades','proveedor_id','categoria_id','nacionalidad_id','descuentos','precio_venta','stock','desc_max','iva_id');
            $crud->callback_column('proveedor_id',function($val,$row){
                return get_instance()->db->get_where('proveedores',array('id'=>$val))->row()->denominacion;
            });
            
            $crud->callback_column('categoria_id',function($val,$row){
                return get_instance()->db->get_where('categoriaproducto',array('id'=>$val))->row()->denominacion;
            });
            
            $crud->callback_column('nacionalidad_id',function($val,$row){
                return get_instance()->db->get_where('paises',array('id'=>$val))->row()->denominacion;
            });
            
             $crud->callback_column('nombre_comercial',function($val,$row){
                return $row->nombre_comercial;
            });
            
             $crud->callback_column('nombre_generico',function($val,$row){
                return $row->nombre_generico;
            });
            
            $crud->callback_column('descmin',function($val,$row){
                return $val.'%';
            });
            
            $crud->callback_column('descmax',function($val,$row){
                return $val.'%';
            });
                                    
            $crud->unset_delete()
                     ->unset_add()
                     ->unset_edit();
            $crud = $crud->render();
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */