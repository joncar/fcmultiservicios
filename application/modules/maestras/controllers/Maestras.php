<?php

require_once APPPATH.'/controllers/Panel.php';    

class Maestras extends Panel {

    function __construct() {
        parent::__construct();             
    }

     public function tipo_contador($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->add_action('Detalles','',base_url('maestras/contadores/').'/');
        $output = $crud->render();            
        $this->loadView($output);
    }

    public function contadores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->where('tipo_contador_id',$x)
             ->field_type('tipo_contador_id',$x)
             ->unset_columns('tipo_contador_id');
        $output = $crud->render();        
        $this->loadView($output);
    }
    
    public function paises($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }

    public function ciudades($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('pais', 'paises', 'denominacion');
        $output = $crud->render();
        $output->relatedlinks = array(array('admin/ciudades', 'Ciudades'));
        $this->loadView($output);
    }
    
    public function proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('tipo_proveedor', 'tipo_proveedores', 'denominacion');
        $crud->required_fields('denominacion', 'ruc', 'direccion', 'ciudad', 'pais_id', 'telefonofijo', 'telefax', 'celular', 'email');
        $crud->unset_fields('created', 'modified');
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        $crud->columns('denominacion', 'tipo_proveedor', 'ruc', 'telefonofijo', 'celular', 'Observacion');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function sucursales($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $crud->set_relation('pais_id', 'paises', 'denominacion');
        $crud->set_relation('ciudad_id', 'ciudades', 'denominacion');
        $crud->set_relation_dependency('ciudad_id', 'pais_id', 'pais');
        $crud->field_type('principal', 'true_false', array('0' => 'No', '1' => 'Si'));

        $crud->callback_before_insert(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $crud->callback_before_update(function($post) {
            if ($post['principal'] == 1)
                $this->db->update('sucursales', array('principal' => 0));
        });
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function clientes($x = '', $y = '', $return = false) {
        $crud = parent::crud_function($x, $y);        
        $crud->field_type('tipo_doc', 'hidden','')
            ->field_type('fecha_nac', 'hidden','')
            ->field_type('pais', 'hidden','')
            ->field_type('ciudad', 'hidden','')
            ->field_type('sexo', 'hidden','')
            ->field_type('celular2', 'hidden','')
            ->field_type('celular3', 'hidden','')
            ->field_type('email', 'hidden','')
            ->field_type('barrio', 'hidden','');
        /*$crud->field_type('sexo', 'dropdown', array('M' => 'M', 'F' => 'F'));
        $crud->set_relation('pais', 'paises', 'denominacion');
        $crud->set_relation('ciudad', 'ciudades', 'denominacion');
        $crud->set_relation('barrio', 'barrios', 'denominacion');
        $crud->set_relation_dependency('ciudad', 'pais', 'pais');
        $crud->set_relation_dependency('barrio', 'ciudad', 'ciudad');*/
        $crud->columns('nombres','apellidos','nro_documento');
        $crud->required_fields('nombres');
        $crud->set_rules('email', 'Email', 'valid_email');
        $crud->callback_after_insert(function($post, $id) {
            $data = array();
            $data['usuario'] = $post['nro_documento'];
            $data['password'] = md5('12345678');
            $data['email'] = $post['email'];
            $data['nombre'] = $post['nombres'];
            $data['apellido'] = $post['apellidos'];
            $data['telefono'] = $post['celular1'];
            $data['status'] = 1;
            $data['admin'] = 3;
            get_instance()->db->insert('user', $data);
            return true;
        });
        if ($x == 'add' || $x == 'insert' || $x == 'insert_validation') {
            $crud->set_rules('nro_documento', 'Cedula', 'required|is_unique[clientes.nro_documento]');
        }
        $crud->unset_fields('created', 'modified');
        if (!empty($x) && !empty($y) && $y == 'json') {
            $crud->unset_back_to_list();            
        }
        if($x=='json_list'){
            $crud->limit(10000);
        }
        $output = $crud->render();
        $output->crud = 'clientes';
        if (!empty($x) && !empty($y) && $y == 'json') {
            $output->json = 'Activo';
            $output->view = 'json';
        }
        $this->loadView($output);
    }
    
    public function monedas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->field_type('primario', 'true_false', array('0' => 'No', '1' => 'Si'));
        $crud->unset_delete();
        $output = $crud->render();
        $output->crud = 'user';
        $this->loadView($output);
    }
    
    public function motivo_salida($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function motivo_entrada($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }

    public function tipo_proveedores($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->unset_delete();
        $this->loadView($crud->render());
    }
    
    public function cuentas($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);        
        $output = $crud->render();
        $this->loadView($output);
    }  
    
    function condiciones(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Condiciones de pago');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Condiciones de pago';
            $this->loadView($crud);
        }
        function estados(){
            $crud = $this->crud_function('','');
            $crud->set_subject('Estados de ocupacion');
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Estados de ocupacion';
            $this->loadView($crud);
        }        
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
