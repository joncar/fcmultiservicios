<?= $output ?>
<script>
	var venta = <?= json_encode($venta) ?>;
	$(document).on('ready',function(){
		$("#field-fecha_credito").val('<?= date("d/m/Y") ?>');
		$("#field-monto_venta").val(venta.total_venta);
		$("#field-entrega_inicial").val(0);
		$("#field-monto_credito").val(venta.total_venta);
	});

	$(document).on('change',"#field-entrega_inicial",function(){
		var credito = parseFloat($("#field-monto_venta").val());
		var inicial = parseFloat($("#field-entrega_inicial").val());
		var total = credito-inicial;
		if(total<0){
			alert("Disculpe, el monto inicial no puede ser superior al monto del crédito");
			$("#field-entrega_inicial").val(0);
			$("#field-monto_credito").val(credito);
		}else{
			$("#field-monto_credito").val(total);
		}
		totalizar();
	});

	$(document).on('change','#field-interes',function(){
		totalizar();
	});

	$(document).on('change','#field-cant_cuota',function(){
		var credito = parseFloat($("#field-total_credito").val());
		var cuotas = parseFloat($("#field-cant_cuota").val());
		if(!isNaN(credito) && !isNaN(cuotas)){
			$("#field-monto_cuota").val(credito/cuotas);
		}
	});

	function totalizar(){
		var interes = parseFloat($('#field-interes').val());
		var credito = parseFloat($("#field-monto_credito").val());
		if(!isNaN(interes) && !isNaN(credito)){
			var total = credito + (credito * (interes/100));
			$("#field-total_credito").val(total.toFixed(0));
		}
	}
</script>

<script>
	function showDetail(id){
		$.post('<?= base_url('movimientos/ventas/ventas_detail/') ?>/'+id+'/',{},function(data){
			emergente(data);
		});
	}
</script>